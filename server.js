const express = require("express");
const app = express();
const { resolve } = require("path");
// Replace if using a different env file or config
const env = require("dotenv").config({ path: "./.env" });

// Set Environment variables.

// Stripe API development Keys
const STRIPE_SECRET_KEY = "sk_test_51I7HUxKi4myTYvAUKbC2Fq8GygqcgOphN2KAKkVH0ToaxHx4tczfnijy89g2zqCiEmT5PNYD66T7mFMj7ADRD4yI00qErFSZv0";
const STRIPE_PUBLISHABLE_KEY = "pk_test_51I7HUxKi4myTYvAU224RrLe9I1ytNNf5CRuLKHi6Tp21iiswbuopfMj5dMD7A20kJ5PP0RuLSs5U80iNYwpf6QsX00DtfFJVXI";

// Client app view location
const STATIC_DIR = ".";

// Server port
const PORT = 5000;

const stripe = require("stripe")(STRIPE_SECRET_KEY);

app.use(express.static(STATIC_DIR));
app.use(express.json());

app.get("/", (req, res) => {
  // Display checkout page
  const path = resolve(STATIC_DIR + "/index.html");
  res.sendFile(path);
});

app.get("/stripe-key", (req, res) => {
  res.send({ publishableKey: STRIPE_PUBLISHABLE_KEY });
});

let orderAmount = 0;
let idFirebase = "";

const calculateOrderAmount = items => {
  // Replace this constant with a calculation of the order's amount
  // You should always calculate the order total on the server to prevent
  // people from directly manipulating the amount on the client

  switch (items[0].id) {
    case 'Alternadom':
      console.log('We buy Alternadom ! ');
      orderAmount = 999;
      idFirebase = "08";
      return true;
    case 'Animaux':
      console.log('We buy Animaux !');
      orderAmount = 499;
      idFirebase = "07";
      return true;
    default:
      console.log(`Oups ¯\\_(ツ)_/¯, error with ${items[0].id}.`);
      return false;
  }
};


app.post("/pay", async (req, res) => {
  const { paymentMethodId, paymentIntentId, items, useStripeSdk } = req.body;

  const currency = "eur";
  
  console.log(req.body);

  // If item exist associate a value item
  if(calculateOrderAmount(items)){
    try {
      let intent;
      if (paymentMethodId) {
        // Create new PaymentIntent with a PaymentMethod ID from the client.
        intent = await stripe.paymentIntents.create({
          amount: orderAmount,
          currency: currency,
          payment_method: paymentMethodId,
          confirmation_method: "manual",
          confirm: true,
          // If a mobile client passes `useStripeSdk`, set `use_stripe_sdk=true`
          // to take advantage of new authentication features in mobile SDKs
          use_stripe_sdk: useStripeSdk,
        });
        // After create, if the PaymentIntent's status is succeeded, fulfill the order.
      } else if (paymentIntentId) {
        // Confirm the PaymentIntent to finalize payment after handling a required action
        // on the client.
        intent = await stripe.paymentIntents.confirm(paymentIntentId);
        // After confirm, if the PaymentIntent's status is succeeded, fulfill the order.
      }
      res.send(generateResponse(intent));
    } catch (e) {
      // Handle "hard declines" e.g. insufficient funds, expired card, etc
      // See https://stripe.com/docs/declines/codes for more
      res.send({ error: e.message });
    }
  }
  else{
    res.send({ error: "¯\\_(ツ)_/¯ Item doesn't exist"});
  }
});

const generateResponse = intent => {
  // Generate a response based on the intent's status
  switch (intent.status) {
    case "requires_action":
    case "requires_source_action":
      // Card requires authentication
      return {
        requiresAction: true,
        clientSecret: intent.client_secret
      };
    case "requires_payment_method":
    case "requires_source":
      // Card was not properly authenticated, suggest a new payment method
      return {
        error: "Your card was denied, please provide a new payment method"
      };
    case "succeeded":
      // Payment is complete, authentication not required
      // To cancel the payment after capture you will need to issue a Refund (https://stripe.com/docs/api/refunds)
      console.log("💰 Payment received!");
      console.log(intent.amount);
      console.log("id firebase" + idFirebase);
      return { clientSecret: intent.client_secret,
        amount : intent.amount,
        idFirebase : idFirebase,
        currency : intent.currency
      };
  }
};

app.listen(process.env.PORT || PORT, () => console.log(`Node server listening on port ${PORT}!`));
